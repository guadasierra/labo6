# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 20:54:22 2021

@author: User
"""

import PID
import time
import matplotlib.pyplot as plt


pid=PID.PID(140,1,10,0.0001) #t_target,kp,ki,kd

pid.sample_time=0.01
N=100 #cantidad de pasos 

temp=293 #temperatura inicial 

#me armo lista para guardar valores para ver la evolucion de
temp_list = [] #temperatura
time_list = [] #tiempo
t_target_list = [] #valor deseado. Este no va a cambiar pero lo necesito para graficar

for i in range(1,N):
    pid.pid(temp) #usa la clase pid del objeto con parametro temp
    power=pid.power #hace el PID y devuelve la potencia 
    temp+=power #En este modelo de juguete el parametro que relaciona la potencia con la temperatura es 1. Esto no es del todo cierto pasa que nos sirve para la simulacion. 
    #la evolucion de la temperatura en la realidad se va a ir viendo con el objeto de los chicos que devuelve una temperatura 
    
    temp_list.append(temp) #agrego a la lista los valores de la temperatura evolucionada
    t_target_list.append(pid.t_target) #valor cte 
    time_list.append(i) #evolucion del tiempo 
    time.sleep(0.01)

plt.plot(time_list, temp_list,"r",label="PID") 
plt.legend(loc="upper right") 
plt.plot(time_list, t_target_list,"g",label="Deseado")
plt.legend(loc="upper right")
plt.xlabel('Tiempo')
plt.ylabel('Temperatura(K)')
plt.title('P=1,I=0.1,D=0.0001')
plt.grid(True)
